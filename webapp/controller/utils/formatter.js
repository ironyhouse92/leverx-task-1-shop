sap.ui.define([], function () {
    "use strict";

    return {
        getFullAddress: function () {
            return jQuery.sap.formatMessage("{0} / {1} / {2}", Object.values(arguments));
        },
	};
});